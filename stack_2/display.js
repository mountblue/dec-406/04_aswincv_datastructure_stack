/*
Name of the project : stack
File name : display.js
Description : print the bracket numbers when the expression is being parsed.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : (a+(b*c))+(d/e)
Output : 1, 2, 2, 1, 3, 3
*/


function display(input) {
    var count = 0;
    var symbol = [];
    var output = [];
    for (var i = 0; i < input.length; i++) {
        if(input.charAt(i)=='(') {
            symbol.push(++count);
            output.push(count);
        }else if (input.charAt(i)==')') {
            output.push(symbol.pop())
        }
    }
    console.log(output);
}


display("(a+(b*c))+(d/e)");
