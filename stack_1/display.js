/*
Name of the project : stack
File name : display.js
Description : Delete the middle element of the stack
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5],3
Output : [1,2,4,5]
*/


function display(arr,k) {
    popElement(arr,k);
    console.log(arr);
}


function popElement(arr,k) {
    var temp;
    temp = arr.pop();
    if(temp == k) {
        return;
    }else {
        popElement(arr,k);
    }
    arr.push(temp);
}

display([1,2,3,4,5],3);
