/*
Name of the project : stack
File name : display.js
Description : finds the nearest smaller number for every element such that the
              smaller element is on left side
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [4,7,2,5,3]
Output : -1, 4, -1, 2, 2
*/


function display(arr1) {
    var arr = arr1;
    arr.reverse();
    var stk = [];
    stk.push(arr[0]);
    var output = [];

    for (var i = 1; i < arr.length; i++) {
        if( stk[stk.length-1] < arr[i]) {
            stk.push(arr[i]);
        }else {
             while(true) {
                 var last = stk.length-1;
                 if( last>=0 && stk[last] > arr[i]) {
                     var j = arr1.indexOf(stk.pop());
                     output[j] = arr[i];
                 }else {
                     stk.push(arr[i]);
                     break;
                 }
             }
        }
    }
    while(true) {
      if(stk.length>0) {
          var j = arr1.indexOf(stk.pop());
          output[j] = -1;
      }else {
        break;
      }
    }
    output.reverse();
    console.log(output);
}

display([4,7,2,5,3]);
