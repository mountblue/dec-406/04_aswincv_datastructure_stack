/*
Name of the project : stack
File name : display.js
Description : maximum absolute difference between nearest left and right
              smaller element of every element in array
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,5,3]
Output : 4,4,3
*/


function find(arr) {
     var result = [];
     for (var i = 0; i < arr.length; i++) {
         if( i == 0 ) {
             result.push( ( Math.abs(arr[i]) > Math.abs(arr[i]-arr[i+1]) ) ?
             Math.abs(arr[i]) : Math.abs(arr[i]-arr[i+1]) );
         }else if (i == arr.length-1) {
             result.push( ( Math.abs(arr[i]-arr[i-1]) > Math.abs(arr[i]) ) ?
             Math.abs(arr[i-1]-arr[i]) : Math.abs(arr[i]) );
         }else {
             result.push( ( Math.abs(arr[i]-arr[i-1]) > Math.abs(arr[i]-
             arr[i+1]) ) ?
             Math.abs(arr[i-1]-arr[i]) : Math.abs(arr[i]-arr[i+1]) );
         }
     }
     console.log(result);
}


find([1,5,3]);
